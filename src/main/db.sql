-- 建表的操作
create database if not exists my_blog;
use my_blog;

--博客表
drop table if exists blog;
create table blog(
    blogId int primary key auto_increment,
    title varchar(1024),
    content mediumtext,-- 这个类型表示能放更长的字符串
    userId int,-- 发布文章作者的id
    postTime datetime -- 发布文章的时间
);

-- 给博客表中插入点数据, 方便测试.
insert into blog values(null, '这是第一篇博客', '从今天开始, 我要认真学 Java', 1, now());
insert into blog values(null, '这是第二篇博客', '从昨天开始, 我要认真学 Java', 1, now());
insert into blog values(null, '这是第三篇博客', '从前天开始, 我要认真学 Java', 1, now());
insert into blog values(null, '这是第四篇博客', '从今天开始, 我要认真学 C++', 2, now());
insert into blog values(null, '这是第五篇博客', '从昨天开始, 我要认真学 C++', 2, now());
insert into blog values(null, '这是第六篇博客', '# 一级标题\n ### 三级标题\n > 这是引用内容', 2, now());


--用户表
drop table if exists user;
create table user(
    userId int primary key auto_increment,
    username varchar(128) unique,     -- 后续会使用这个用户名进行登录,用户名不能重复
    password varchar(128)
       -- 这里应该再插入用户的文章数目和类别以及GitHub地址
);

insert into user values(null, 'zhangsan', '123');
insert into user values(null, 'lisi', '123');
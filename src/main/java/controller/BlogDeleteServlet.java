package controller;

import model.Blog;
import model.BlogDao;
import model.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

/**
 * Creared with IntelliJ IDEA.
 * Description:处理删除博客请求
 * User:yxd
 * Date:2022-06-05
 * Time:19:55
 */
@WebServlet("/blogDelete")
public class BlogDeleteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //删除此博客
        req.setCharacterEncoding("utf8");
        //就可以从请求中取出对应的博客id,然后进行删除就好了
        HttpSession session = req.getSession(false);
        if(session == null){
            //当前未登录,不能提交博客
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前用户未登录,不能删除博客!");
            return;
        }
        //从会话中取出当前用户信息
        User user = (User) session.getAttribute("user");
        //判断用户信息是否存在
        if(user == null){
            //当前用户不存在,表示未登录
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前用户未登录,不能删除博客!");
            return;
        }
        //再取出这篇文章
        String blogId = req.getParameter("blogId");
        if(blogId == null || "".equals(blogId)){
            //当前博客不存在
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前blogId参数不正确不能删除!");
            return;
        }
        //然后获取到要删除的博客信息
        BlogDao blogDao = new BlogDao();
        Blog blog = blogDao.select(Integer.parseInt(blogId));
        //判断文章是否存在
        if(blog == null) {
            //当前博客不存在,无法删除
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前博客不存在不能删除!");
            return;
        }
        //再判断当前博客作者和当前用户是不是同一个人,两次进行校验(客户端和服务器)
        if(user.getUserId() != blog.getUserId()){
            //不是同一个用户无法删除
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前登录的用户不是作者,没有权限删除!");
            return;
        }
        //此时就可以进行删除了
        blogDao.delete(Integer.parseInt(blogId));
        //然后再重定向到博客列表页
        resp.sendRedirect("blog_list.html");
    }
}

package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.Blog;
import model.BlogDao;
import model.User;

import javax.jws.soap.SOAPBinding;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.List;

/**
 * Creared with IntelliJ IDEA.
 * Description:通过这个类来完成/blog路径的请求
 * User:yxd
 * Date:2022-06-04
 * Time:21:23
 */
@WebServlet("/blog")
public class BlogServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    //而获取博客列表和获取博客详情是不一样的,因此这里需要通过参数来区分这两种,一个是获取全部的内容每一个是通过博客Id来获取一片博客,看有无参数
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //这里就需要把数据库中的数据查询出来,然后转换成JSON格式,输出就可以了
        //通过封装数据库操作的类来进行查询
        BlogDao blogDao = new BlogDao();
        //防止乱码,这个和下面的顺序不能颠倒,先设置字符,再输出
        resp.setContentType("application/json;charset=utf8");

        //因此这里就先来尝试获取req中的blogId参数,如果能获取到,就请求博客详情,如果获取不到就证明是获取博客列表内容
        String param = req.getParameter("blogId");
        if(param == null){
            //表示没有参数,就是获取博客列表内容
            //把查询的结果放到一个数组中
            List<Blog> blogs = blogDao.selectAll();
            //然后通过objectMapper把这个转换成JSON格式,再进行输出
            String respJson = objectMapper.writeValueAsString(blogs);
            //然后输出就可以了
            resp.getWriter().write(respJson);
        }else {
            //转换成整数
            int blogId = Integer.parseInt(param);
            //然后获取到这个博客内容
            Blog blog = blogDao.select(blogId);
            //然后再转换
            String respJosn = objectMapper.writeValueAsString(blog);
            resp.getWriter().write(respJosn);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //这个方法就处理发布博客的请求内容
        //此处一定要指定好按照哪种编码来解析
        req.setCharacterEncoding("utf8");
        //就可以从请求中取出对应的标题和正文,以及从会话中取出当前作者的id,然后再插入到数据库当中,插入完成跳转到博客列表页就好了
        HttpSession session = req.getSession(false);
        if(session == null){
            //当前未登录,不能提交博客
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前用户未登录,不能提交博客!");
            return;
        }
        //从会话中取出当前用户信息
        User user = (User) session.getAttribute("user");
        //判断用户信息是否存在
        if(user == null){
            //当前用户不存在,表示未登录
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前用户未登录,不能提交博客!");
            return;
        }
        //此时就表示当前用户登录成功了,可以提交博客
        //获得标题和正文
        String title = req.getParameter("title");
        String content = req.getParameter("content");
        if(title == null || "".equals(title) || content == null || "".equals(content)){
            //当前提交的信息有问题,不能提交
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("提交博客失败!缺少必要的参数");
            return;
        }
        //现在就可以将这篇博客插入到数据库当中去了
        //这里只需要插入标题和正文以及用户Id就可以了其他两个是系统自己确定的
        Blog blog = new Blog();
        blog.setTitle(title);
        blog.setContent(content);
        blog.setUserId(user.getUserId());
        //进行插入
        BlogDao blogDao = new BlogDao();
        blogDao.insert(blog);
        //插入完成之后,进行跳转页面到博客列表页
        resp.sendRedirect("blog_list.html");
    }
}

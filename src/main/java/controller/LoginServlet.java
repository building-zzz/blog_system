package controller;

import com.fasterxml.jackson.databind.ObjectMapper;
import model.User;
import model.UserDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Creared with IntelliJ IDEA.
 * Description:这里处理登录页面的请求
 * User:yxd
 * Date:2022-06-05
 * Time:13:13
 */
@WebServlet("/login")
public class LoginServlet extends HttpServlet {
    private ObjectMapper objectMapper = new ObjectMapper();
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //另外Servlet并不是根据utf8的格式来解析的,因此要是不处理字符编码的问题的话,很有可能出现乱码的问题,因此把请求和响应的字符编码都处理一下
        req.setCharacterEncoding("utf8");
        resp.setCharacterEncoding("utf8");

        //这里从请求中取出username和password
        String username = req.getParameter("username");
        String password = req.getParameter("password");
        //判断是不是为空的用户名为密码
        if(username == null || "".equals(username) || password == null || "".equals(password)){
            //此时用户名或密码为空,那么此时就不能登录,需要提示用户用户名或密码为空
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("当前的用户名或密码为空!");
            return;
        }
        // 来判断是否正确,从数据库中通过用户名取出这个用户,如果能取到的话,或者密码正确的话,就表示输入是成功的
        UserDao userDao = new UserDao();
        User user = userDao.selectByName(username);
        if(user == null || !user.getPassword().equals(password)){
            //用户不存在或者密码输入错误
            resp.setContentType("text/html;charset=utf8");
            resp.getWriter().write("用户名或密码错误!");
            return;
        }
        // 如果正确,就登录成功创建会话,如果失败就登录失败
        //此时就应该创建一个会话来保存当前用户的信息
        HttpSession httpSession = req.getSession(true);
        //然后把用户的信息存储到这个会话当中去
        httpSession.setAttribute("user",user);

        //然后再给页面会返回一个重定向报文,跳转到博客列表页
        resp.sendRedirect("blog_list.html");
    }
    //用这个方法来检验当前的登录状态
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //获取到当前的会话,看会话是不是空的,或者会话中有没有内容,如果两个都有就表示登录成功了
        resp.setContentType("aoolication/json;charset=utf8");
        HttpSession httpSession = req.getSession(false);
        if(httpSession == null){
            //会话为空,表示这个会话不存在,那么就返回一个默认的user对象(可以设置一下默认值,也就是空的
            User user = new User();
            //然后把这个对象转换成json字符串,然后放到body里面
            resp.getWriter().write(objectMapper.writeValueAsString(user));
            return;
        }
        //如果会话不为空,但是里面没有东西,也是不行的
        User user = (User) httpSession.getAttribute("user");//在哈希表的结构中去查找
        if(user == null){
            //表示当前会话里面没有用户
            resp.getWriter().write(objectMapper.writeValueAsString(user));
            return;
        }
        //到这里就表示已经是登录成功了的,直接把当前对象的内容返回就好了,另外不要把密码返回回去,应该把密码设置为空
        user.setPassword("");
        resp.getWriter().write(objectMapper.writeValueAsString(user));
    }
}

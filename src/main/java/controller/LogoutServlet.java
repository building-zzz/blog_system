package controller;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * Creared with IntelliJ IDEA.
 * Description:处理注销按钮的请求
 * User:yxd
 * Date:2022-06-05
 * Time:15:52
 */
@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf8");
        //先找到当前用户的会话
        HttpSession session = req.getSession(false);
        //判断当前会话是否存在
        if(session == null){
            //当前用户未登录,不能注销
            resp.getWriter().write("当前用户尚未登录,不能注销!");
            return;
        }
        //用户登录了,就把当前会话中的用户删除掉就好了,这里需要删除掉会话或者会话其中一个就可以了,然后就会触发验证用户是否登录的条件判断了,就会显示登录失败了
        session.removeAttribute("user");
        //然后重定向到登录页面
        resp.sendRedirect("blog_login.html");
    }
}

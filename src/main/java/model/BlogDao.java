package model;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Creared with IntelliJ IDEA.
 * Description:这个类用于封装博客表的基本操作(增删改查)
 * User:yxd
 * Date:2022-06-04
 * Time:17:10
 */
public class BlogDao {
    //要实现的功能:
    //1.往博客表里面插入一个博客
    public void insert(Blog blog){
        //插入一篇博客
        //这里就是JDBC的基本代码的写法:先建立连接,然后进行sql语句的书写,再将sql语句放入数据库,再执行sql语句,最后再释放资源
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            //sql语句
            String sql = "insert into blog values(null,?,?,?,now())";
            statement = connection.prepareStatement(sql);
            statement.setString(1,blog.getTitle());
            statement.setString(2,blog.getContent());
            statement.setInt(3,blog.getUserId());
            //执行连接
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            //关闭连接释放资源
            DBUtil.close(connection,statement,null);
        }
    }


    //2.能够获取到博客表中的所有博客的信息(用于在博客列表页,这里不需要获取所有的博客内容)
    public List<Blog> selectAll(){
        List<Blog> blogs = new ArrayList<>();
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            //sql语句  在这里查询的时候就需要给这些博客按时间进行排序,保证最新的博客是在最上面的,而不是乱序排列的,因此就需要加上order by desc
            String sql = "select * from blog order by postTime desc";
            statement = connection.prepareStatement(sql);
            //执行连接
            resultSet = statement.executeQuery();
            //遍历结果集
            while(resultSet.next()){
                Blog blog = new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                //这里要对全文内容进行摘要截取,如果超过50个字就取其前50个字加...没超过就是原来的长度
                String content = resultSet.getString("content");
                if(content.length() > 50){
                    content = content.substring(0,50) + "...";
                }
                blog.setContent(content);
                blog.setUserId(resultSet.getInt("userId"));
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                blogs.add(blog);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {

            DBUtil.close(connection,statement,resultSet);
        }
        return blogs;

    }

    //3.能够根据博客id获得到指定的博客内容(用于博客详情页)'
    public Blog select(int blogId){
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        try {
            connection = DBUtil.getConnection();
            //sql语句
            String sql = "select * from blog where blogId = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            //执行连接
            resultSet = statement.executeQuery();
            //遍历结果集 由于这里只有一条记录(是使用主键进行查询的),因此就不需where而只需要if就可以了
            if(resultSet.next()){
                Blog blog = new Blog();
                blog.setBlogId(resultSet.getInt("blogId"));
                blog.setTitle(resultSet.getString("title"));
                blog.setContent(resultSet.getString("content"));
                blog.setUserId(resultSet.getInt("userId"));
                blog.setPostTime(resultSet.getTimestamp("postTime"));
                //如果找到了就直接返回.没找到就不返回
                return blog;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {

            DBUtil.close(connection,statement,resultSet);
        }
        //没找到
        return null;
    }

    //4.从博客列表页根据博客id进行删除博客
    public void delete(int blogId){
        Connection connection = null;
        PreparedStatement statement = null;
        try {
            connection = DBUtil.getConnection();
            //sql语句
            String sql = "delete from blog where blogid = ?";
            statement = connection.prepareStatement(sql);
            statement.setInt(1,blogId);
            //执行sql
            statement.executeUpdate();

        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(connection,statement,null);
        }
    }
}
